# python_bluetooth

#### 介绍
python bluetooth pypi page

#### 软件架构
基于python 的pypi page,实现使用bluetooth le


#### 安装教程
1. 安装 net4.6
##
   在本工程setup tool 目录下的[net4.6](setuptool/NDP46-KB3045557-x86-x64-AllOS-ENU.exe) ,若未安装可自行下载.
##
2. python 3.8

3.1 [在线安装](https://pypi.org/project/BluetoothLEAiqi/) ：
> pip install BluetoothLEAiqi

3.2 离线安装
##
下载最新版[BluetoothLEAiqi-x.x.x.tar.gz](https://gitee.com/liuming_aiqi/python_bluetooth/releases/)
>pip install BluetoothLEAiqi-x.x.x.tar.gz


#### 使用说明
使用可参考 pypi_page/BluetoothLEAiqi_Example 目录下app_demo.py,需要将bluetoothle_app.py 放置工程目录下



#### 使用要求

1.  基于windwos 10 V2004 
2.  需要 [.net4.6](setuptool/NDP46-KB3045557-x86-x64-AllOS-ENU.exe) 支持，
3.  本机蓝牙适配器支持bluetooth LE 

#### 提示
目前仅支持win 10 下的特定蓝牙服务及特征

#### 参与贡献

1.  Fork 