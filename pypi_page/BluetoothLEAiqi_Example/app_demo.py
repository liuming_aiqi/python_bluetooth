import time

import bluetoothle_app as le_driver


my_xl_name = "OnBot_XL_M"
my_xl_mac = "00312E0120AA"
my_onebot = "Onebot"

'''

蓝牙连接状态监听

'''

def Ble_connect_stats_handle(sender, par):
    print("stats  ",par)
    if par == 0:
        print("connect ok")
        le_driver.Ble_OneBot_Mesh_Login()
    elif par == 2:
        print("connect err")
        le_driver.BluetoothLE_DIsconnect()
    else:
        print("disconnect")

'''

蓝牙接受数据监听

'''

def Ble_value_change_handle(sender, par):
    print("rev value len:",len(par))
    print(par[0])

'''

播放特定音效

'''
def Play_Sound_Test_Report(sound_id):

    sound_report = [0x00, 0x02, 0x02, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        , 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0]
    sound_report[4]= sound_id

    sound_report[19] = 0
    sound_report[18]=le_driver.Aiqi_Report_XorCheck(sound_report)
    sound_report[19] = 0xaa

    le_driver.BluetoothLE_Send(sound_report)


if __name__ == '__main__':
    le_driver.BluetoothLE_INIT(Ble_connect_stats_handle,Ble_value_change_handle)
    # le_driver.BluetoothLE_Connect(name=my_xl_name)
    le_driver.BluetoothLE_Connect(name=my_onebot)
    

    while True :
        time.sleep(5)
        Play_Sound_Test_Report(0x12)
